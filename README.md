Сервис создания мнемоник

Описание
Нужно разработать разновидность Parody generator для создания мнемоник для чисел произвольной длинны. Цель приложения - упростить запоминание констант.
Приложение должно функционировать следующим образом:
При запуске приложения, оно получает доступ к тексту, на основе которого будет построен индекс слов 
После того, как индекс построен пользователям становиться доступен простой web интерфейс. Интерфейс должен содержать единственное поле, принимающее только числовые значения
На выход пользователь должен получить предложение, соответствующее переданному числу.  Соответствие определяется следующим образом 
Из числа последовательно выбираются цифры
В индексе слов выбирается слово, чья длинна равна выбранной цифре. Если это первый шаг алгоритма, слово может быть выбрано произвольно из всего набора слов подходящей длинны. Иначе слово должно быть наиболее вероятным из всех подходящих по длине слов, которые могут стоять следом за словом, выбранным на предыдущем шаге.
Знаки препинания в индекс слов входить не должны
Технические требования
Текст для индекса может быть представлен в виде файла или УРЛ. 
Кодировка всех текстов UTF
Языки: английский и русский
Время отклика t < 20 сек
Длинна числа L <= 30 символов
Http сервер произвольный

Документация и полезная информация
Spray can as an option for http layer https://github.com/spray/spray-can
Цепи Маркова  https://www.dartmouth.edu/~chance/teaching_aids/books_articles/probability_book/Chapter11.pdf
Parody  generator https://en.wikipedia.org/wiki/Parody_generator
Пример https://en.wikipedia.org/wiki/Dissociated_press
