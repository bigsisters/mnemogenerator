package webUI

import java.io.{FileInputStream, FileOutputStream, File}

import akka.actor.Actor
import mnemogenerator.MnemoGenerator
import spray.routing._
import spray.http._
import MediaTypes._
import HttpCharsets._


class MyServiceActor extends Actor with MyService {

  def actorRefFactory = context

  def receive = runRoute(myRoute)
}

trait MyService extends HttpService {
  var myChain = MnemoGenerator.feedSourceText(new FileInputStream("./src/main/resources/texts/idiot.txt"))

  def sentenceTemplate(sentence:String) =
    <html>
      <head>
        <meta charset="utf-8"></meta>
        <link href="http://fonts.googleapis.com/css?family=Waiting+for+the+Sunrise" rel="stylesheet" type="text/css"/>
        <link href="style2.css" rel="stylesheet"></link>
      </head>
  <body>
    <div id="typedtext">{sentence}</div>
    <div><button><a id="link-to-index" href="/generate">Назад</a></button></div>
  </body>
    <script src="typewrite.js"></script>
  </html>.toString()

  val myRoute = {
    path("") {
      getFromResource("web/index.html")
    } ~ {
        getFromResourceDirectory("web")
      } ~
    path("generate") {
      getFromResource("web/generateview.html")
    } ~
    pathPrefix("sentence") {
      pathEnd {
        post {
          entity(as[MultipartFormData]) {
            formData => {
              val constantField = formData.fields.filter(field => field.name.getOrElse("") == "constant").head
              val numberString = constantField.entity.data.asString
              try {
                numberString.toDouble
                val generatedSentence = myChain.buildNewText(numberString).mkString(" ")
                complete(HttpEntity(ContentType(`text/html`,`UTF-8`), sentenceTemplate(generatedSentence) ))
              } catch {
                case e:NumberFormatException => redirect("/", StatusCodes.NotModified)
              }
            }
          }
        }
      }
    } ~
      pathPrefix("file-upload") {
        pathEnd {
          post {
            entity(as[MultipartFormData]) {
              formData => {
                val tmpFile = File.createTempFile("upload", ".txt", new File("/tmp"))
                val output = new FileOutputStream(tmpFile)
                formData.fields.foreach(f => output.write(f.entity.data.toByteArray))
                output.close()
                myChain = MnemoGenerator.feedSourceText(new FileInputStream(tmpFile))
                redirect("/generate", StatusCodes.Found)
            }
          }
        }
      }
  }
}
}